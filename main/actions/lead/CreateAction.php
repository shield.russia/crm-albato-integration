<?php

namespace app\actions\lead;

use app\forms\LeadForm;
use Yii;
use yii\base\Action;
use yii\db\Exception;
use yii\helpers\Json;

class CreateAction extends Action
{

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $request = Yii::$app->request;

        if (!$request->isPost){
            throw new \Exception('Неподдерживаемый тип запроса', 405);
        }

        if (!Yii::$app->request->post()){
            throw new \Exception('Нет данных в запросе', 400);
        }

        $order = new LeadForm();

        if ($order->load(Yii::$app->request->post(), '')) {
            if (!$order->validate()) {
                return Json::encode($order->getErrors());
            }

            try {
                $customer = $this->saveCustomer($order);
                $this->saveOrder($order, $customer['id']);

            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            return 'FALSE';
        }

        return 'OK';
    }

    /**
     * @param LeadForm $order
     * @param $customerId
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function saveOrder(LeadForm $order, $customerId)
    {
        $order = Yii::$container->get('OrderApi')
            ->save($order->getOrderData($customerId))
            ->getData();

        if (empty($order['orderId'])) {
            throw new Exception('Не удалось сохранить заказ');
        }

        return $order;
    }

    /**
     * @param  LeadForm $order
     * @return array
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private function saveCustomer(LeadForm $order)
    {
        $customer = Yii::$container->get('CustomerApi')
            ->save($order->getCustomerData())
            ->getData();


        if (empty($customer['id'])) {
            throw new Exception('Не удалось сохранить клиента');
        }

        return $customer;
    }


}