<?php

namespace app\forms;

use Yii;

class LeadForm extends \yii\base\Model
{

    public $phone;
    public $comment;
    public $name;
    public $source;

    const ADDRESS = ',630000,Новосибирская обл,,Новосибирск г,,,,,';


    public function rules()
    {
        return [
            [['phone', 'comment', 'name', 'source'], 'safe'],
            [['phone', 'source'], 'required']
        ];
    }

    protected function getPhone(){
        return preg_replace('![^0-9]+!', '', $this->phone);
    }

    /**
     * Возвращает данные для сохранения кастомера
     * @return array
     */
    public function getCustomerData()
    {
        return [
            'is_company' => false,
            'phones' => [
                [
                    'type' => 1,
                    'phone_number' => $this->getPhone()
                ],

            ],
            'info' => [
                'lastname' => null,
                'firstname' => $this->name,
                'midname' => null,
            ],
        ];
    }

    /**
     * Возвращает данные для сохранения заказа
     * @param $customerId
     * @return array
     */
    public function getOrderData($customerId)
    {
        return [
            'customerId' => $customerId,
            'userId' => 0,
            'params' => [
                'comment' => $this->comment,
            ],
            'requests' => [
                [
                    'typeId' => 1,
                    'address' => self::ADDRESS,
                    'source' => (int)$this->source
                ]
            ]
        ];
    }
}