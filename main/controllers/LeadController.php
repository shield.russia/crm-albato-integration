<?php

namespace app\controllers;

use app\actions\lead\CreateAction;
use yii\web\Controller;

class LeadController extends Controller {

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'create' => [
                'class' => CreateAction::class,
            ],
        ];
    }
}